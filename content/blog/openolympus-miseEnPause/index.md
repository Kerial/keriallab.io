---
title: OpenOlympus - Mise en pause
subtitle: Mais pas indéfiniment
date: 2020-01-09
---

Voilà quelques temps que je n'ai pas mis d'article sur ce blog.

Les choses ont changé depuis l'été dernier.
Les questions techniques qui se sont posées n'ont toujours pas été résolues.
Des issues n'avancent pas.

<!--more-->

Je ne préfère pas prendre de décision seul pour l'ensemble des personnes du projet, sauf qu'au final, j'ai l'impression que personne n'en prend. Ou plutôt, j'ai l'impression d'être seul à bord du bateau! ^^'
Donc j'ai préféré mettre le projet en pause de mon côté.
Je vais voir si des choses cont être faites d'ici à quelques mois.
Si oui, tant mieux et je m'y remettrai sûrement !
Si non, alors tant pis. Cela indiquera que, pour continuer ce projet, il faudra que je le fasse seul.
Ce n'est pas un problème, mais comme j'ai actuellement d'autres projets en cours, il va falloir attendre un peu pour celui-ci! :)
