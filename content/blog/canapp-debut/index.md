---
title: Can'App - Début du projet
subtitle: Premières implémentations front
date: 2019-10-04
---

OK. Le choix de la technologie est fait.
Etant donné que la base de données d'OpenStreetMap propose le tag "dog", pas besoin de back!
On va donc se concentrer uniquement sur la partie front.
Et ça sera en **React-Native**.

Ne connaissant rien à React-Native, et quasiment rien à Javascript, c'est parti pour les tutoriels de OpenClassrooms !

A finir
