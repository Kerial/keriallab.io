---
title: Actualisation du blog
subtitle: Après plus de deux ans
date: 2022-06-14
---

Je reviens sur ce blog après quelques temps passés (2 ans et demi !).
Et durant ces quelques temps, des projets ont été réalisés.
J'en ai actuellement deux qui me viennent en tête, deux qui ont été aboutis :
* ShowMaster: un jeu, disponible sur Android, réalisé dans le cadre d'une GameJam
* OutilsProfs: un ensemble d'outils dédiés aux professeurs des écoles

<!--more-->

Je vais expliciter ShowMaster ici, et garder OutilsProfs pour une liste d'articles.

ShowMaster a été réalisé lors d'une GameJam, d'une durée de 1 mois.
Le principe : 1 mois pour réaliser un jeu de bout en bout, comprennant donc le code, mais également le son et les images.
Tout a été réalisé par mes soins.

*Pour contextualiser, je me suis inscrit à cette GameJam suite à la réalisation d'un autre petit jeu sur Android.
J'ai ainsi réalisé un prmeier jeu avec Unity, que j'ai publié sur le Play Store, puis j'ai eu envie d'avoir un véritable but derrière, pas juste faire un jeu pour en faire un. La GameJam m'a donné cette opportunité.*

Le thème : "Game Show"

Ayant déjà utilisé Unity mais n'ayant pas spécialement aimé, j'ai préféré partir sur un autre outils de développement.
Je me suis ainsi tourné vers GodotEngine, un moteur de jeu 2D et 3D gratuit et open-source.
Le langage ressemble beaucoup au Python, donc m'était assez familier.

Au bout d'un mois (2 soirs par semaine, puis toute la soirée les 3 derniers jours + la dernière nuit quasiment complète), voici le résultat : https://kerial.itch.io/show-master

Ce fut une expérience intéressante, notamment pour :
* La découverte de nombreux outils permettant de faire de la création audiovisuelle.
* La découverte de GodotEngine. Je me tournerais vers ce moteur si je devais refaire de de la création de jeu !
* La stimulation: même si je ne connaissais personne participant à cette Jam et que je ne savais pas si beaucoup de monde y participait, c'était très motivant! L'esprit humain est facilement motivable :)