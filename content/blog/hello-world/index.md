---
title: Hello Everyone
subtitle: Presentation of this blog
date: 2019-02-13
---

The purposes of this blog are :

- to give some hints about problems rencountered during development
- to give an overview of the evolution of the projects I participate

<!--more-->

At the moment I write these lines, to major project I'm working on is [OpenOlympus](https://gitlab.com/openolympus/openolympus).
I'll write some posts about this project :

- The frameworks used (libgdx and artemis-odb in particular)
- Some patterns used (description, utility and example)
- The evolution of the project
- Other stuff ?

Let's go! :)
