---
title: OutilsProfs - Présentation
subtitle: Génèse du projet
date: 2022-06-14
---

Ma femme est professeure des écoles.
Les outils proposés par l'éducation nationale sont, certes, bien faits, mais ne correspondent pas forcément aux besoins des utilisateurs.
Ma femme doit ainsi adapter les résultats et documents qu'elle produit au fur et à mesure de l'année avec ses élèves afin de les faire correspondre au logiciel.
Or, un logiciel qui répond aux besoins de ses utilisateurs, n'a-t-il pas le rôle inverse ? Ne doit-il, idéalement, prendre les données brutes de l'utilisateur, et les lui formater afin de mettre en valeur les informations importantes ?

Afin d'établir un diagnostic plus précis de la situation, j'ai effectué un sondage auprès de professeurs des écoles, contactés via plusieurs groupes Facebook.
J'ai ainsi cherché à recueillir un maximum de besoins, puis je les ai synthétisés et ai sorti les besoins les plus récurrents.
J'ai ensuite créé un sondage avec uniquement ces besoins, et demandant aux sondés de mettre un note pour chaque besoin, indiquant l'importance de celui-ci.
J'ai ainsi pu dégager la fonctionnalité principale de mon futur SAAS : **Listing des élèves + coche des compétences acquises + impression du document par élève**

Une fois le besoin principal déterminé, on commence à réfléchir à une architecture !