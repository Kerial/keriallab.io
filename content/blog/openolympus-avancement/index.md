---
title: OpenOlympus - Etat d'avancement
subtitle: Dicussion avec l'équipe - Problème de sérialisation
date: 2019-08-06
---

Ce post pour parler de l'état d'avancement du projet.
Etant donné qu'il ne s'agit pas d'un tuto ou d'une explication technique, j'écris ce post en français.
C'est surtout pour garder une trace de l'évolution du projet.

Actuellement, une disucssion est en cours dans l'équipe sur une refonte totale du projet.
Il s'agirait notamment de repartir quasiment de zéro, avec cependant la connaissance des problèmes rencontrés.

De mon côté, je ne suis pas spécialement pour.
En effet, cela fait quasiment 2 ans que le projet a été démarré, et le jeu se développe lentement.
J'ai bien peur que repartir de zéro ne fasse que décourager et au final mener à l'abandon du projet.
Je serais donc pour continuer à développer en Java, et participer au "nouveau" projet si celui-ci se met en place.
Il faudrait alors définir la technologie souhaitée et la marche à suivre.


Par rapport au projet en Java, une question en actuellement en cours.
En effet, on aimerait sérialiser l'ensemble du jeu en JSON.
Le problème est que :
  * le format de carte utilisé par LibGdx est le TMX. Il est ainsi facile de charger une carte dans LibGdx si celle-ci est dans ce format.
  * il n'existe actuellement pas de désérializer natif LibGdx pour du JSON
  
Ainsi : 
  * soit on reste sur du TMX. C'est moyennement propre car on a quasiment tout en JSON. Il faudrait ainsi gérer les deux formats. Assez contraignant.
  * soit on implémente un désérializer pour créer une TiledMap à partir d'un fichier JSON.
  * soit on implémente notre propre gestionnaire d'affichage qui est ainsi totalement décorrélé de l'objet TiledMap.
  
  
Je suis à la recherche d'une meilleure solution.
