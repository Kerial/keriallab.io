---
title: Architecture of a React-Native project
subtitle: One way of building it
date: 2019-10-03
---

Here is an article about the architecture I choose to build my React-Native application.
This architecture was conceived from the read of different articles found on the web.
I tried to pick up the ideas I found the best to make my own architecture.
I think it's a pretty common architecture, but in the doubt, I share this! :)

For my project, I have :
* To communicate with a Rest API
* To have objects shared with all the components of my project
* To have specificity for Android
* To have images, sounds, etc. (assets)

In order to have that, my final architecture looks like :

.
+-- android
+-- api
|   +-- MyApi.js
+-- assets
+-- components
|   +-- MyComponent1
|   |   +-- index.js
|   |   +-- style.js
|   +-- MyComponent2
|   |   +-- index.js
|   |   +-- style.js
+-- navigation
|   +-- Navigation.js
+-- store
|   |   +-- reducers
|   |   |   +-- index.js
|   |   |   +-- myReducer1.js
|   |   |   +-- myReducer2.js
|   +-- configureStore.js
+-- utils
|   +-- utilFile1.js
|   +-- utilFile2.js
+-- App.js

With the content :
* for components/MyComponent1 :
  * index.js : the code of my React-Native component
  * style.js : the style of that component
* for the store :
  * reducers/index.js : 
  ```javascript
  import { combineReducers } from 'redux';
  import myReducer1 from './myReducer1';
  import myReducer2 from './myReducer2';

  export default combineReducers({
    myReducer1,
    myReducer2
  });
  ```
  * configureStore.js
  ```javascript
  import { createStore } from 'redux';
  import reducer from './reducers/index';

  export default createStore(reducer);
  ```

In case it could help...

[edit 13/01/2019]: After reading some documentation and tutoriels, here is a github project presenting a boilerplate for react-native projects : https://github.com/thecodingmachine/react-native-boilerplate.
