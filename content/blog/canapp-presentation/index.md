---
title: Présentation du projet Can'App
subtitle: Génèse - Idée et premières questions
date: 2019-09-10
---

Durant l'été, j'étais en vacances dans le sud de la France.
Avec ma copine, nous étions en vadrouille avec notre chien.
Quelques jours par ci, quelques jours par là...
Et chaque jour, une question récurrente : les chiens sont-ils autorisés dans ce lieu ?

<!--more-->

Pour obtenir la réponse, pas trop de choix : demander par téléphone (si ça décroche) ou s'y rendre.
Dans un monde où l'information est partout, il nous manquait cette information pourtant simple.
D'où l'idée de créer une application qui permettrait de savoir pour n'importe quel lieu public si les chiens sont acceptés.

Se posent alors les questions relatives à la réalisation :
* Quel est le besoin fondamental auquel répond l'application ? Comment y répondre ?
* Quelles sont les plateformes ciblées ?
* Quelle technologie pour la partie front ?
* Ai-je besoin d'une partie back ? Si oui, quelle technologie ? Et quels frais liés ?  
  
  
#### Quel est le besoin fondamental auquel répond l'application ? Comment y répondre ?
Le but de l'application est de permettre à un utilisateur de savoir si un lieu accepte les chiens.
Deux comportements sont envisagés :
* L'utilisateur sait où il souhaite aller. Il veut savoir si, dans ce lieu, les chiens sont autorisés.
* L'utilisateur est par exemple dans une ville et cherche un endroit où se rendre qui accepte les chiens.
Ces deux comportements sont très différents. 
En effet, dans le premier cas, l'utilisateur va donner un nom d'endroit. L'objectif de l'application est alors d'indiquer si cet endroit accepte les chiens.

Dans le deuxième cas, l'utilisateur cherche un endroit "au hasard". 
Au hasard ? Pas tellement... Il sait déjà qu'il veut aller dans un café, un restaurant, un parc...
Autrement dit, il faut chercher les lieux du type voulu et proches de l'utilisateur.

Le deuxième comportement semble ainsi être un comportement avancé du premier.
Cette application étant la première que je développe, autant commencer simple...
On se limite ainsi au premier comportement ! Du moins, pour le moment...

#### Quelles sont les plateformes ciblées ?
Pas de spécificité au départ.
J'aimerais quelque chose qui soit compatible avec Android et Apple (le reste, je n'y pense pas).
Je ne compte pas "faire de l'argent" avec cette application. Or :
* Un compteur développeur Android, c'est un paiement, une fois, et c'est réglé. 25$.
* Un compteur développeur Apple, c'est (de ce que j'ai lu), un abonnement annuel. 99$.

Bon, bah le choix est vite fait...
On va faire quelque chose qui sera le plus portable possible, mais si il faut se spécifier sur du Android, soit.

#### Quelle technologie pour la partie front ?
Le but est de faire quelque chose de cross-platform.
Après quelques recherches, du benchmarking, des "pros and cons", des articles divers et variés, quelques technologies ressortent.
Mais le choix final se porte sur **React-Native**.
Du Javascript ! Tant mieux, j'en ai quasiment jamais fait! :D
Moi qui cherchais un projet pour apprendre de nouvelles choses, je me sers! :p

#### Ai-je besoin d'une partie back ? Si oui, quelle technologie ? Et quels frais liés ?
Au départ, je me dis que j'ai besoin d'un back.
J'aimerais en effet offrir la possibilité de mettre un commentaire sur les lieux.
De pouvoir peut-être se créer un compte pour avoir des lieux favoris. Etc.

Je commence alors à mettre en place un back en NodeJs.
Création de modèles, de routes, de controllers, le tout séparés par version.
Puis les tests avec Mocha et Chaï.  
Et puis j'en parle à un ami qui me souligne le fait que, si je crée ma base de données en plus, il va falloir gérer les nouvelles réglementations sur le RPGD, l'authentification, et plein de petits trucs sympas dans le genre.
Bref, c'est du lourd. Et ça nécessite de louer un serveur quelque part qui soit disponible --> frais supplémentaires.
Et pour quel apport ?
En pesant le pour et le contre, je me ravise.  
Il va falloir penser à une autre solution...
