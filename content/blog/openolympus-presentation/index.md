---
title: Presentation of OpenOlympus
subtitle: Genese of the project
date: 2019-02-14
---

Do you ever heard about "Zeus: Master of Olympus" or its extension "Poseidon: Master of Atlantis" ?
For me, it's THE game I played when I was a child.
And, a few years ago, I spoke about it with a friend who played it as a child too.
We decided to play together and I rediscovered this game which was still wonderful !

{{< gallery caption-effect="fade" hover-effect="grow" >}}
  {{< figure thumb="-thumb" link="/img/splashScreen.jpg" alt="Image from masterofOlympus.com" >}}
  {{< figure thumb="-thumb" link="/img/fishBoat.jpg" alt="Image from masterofOlympus.com" >}}
  {{< figure thumb="-thumb" link="/img/city.jpg" alt="Image from masterofOlympus.com" >}}
{{< /gallery >}}
{{< load-photoswipe >}}

BUT, the same thoughts came again :

- "What if I could close this specific building to have more available workers ?" <!--more-->
- "What if the map was unlimited ?"
- "What if I could control a god ?"
- and so on...

Per chance, now, I'm a developer ! :tada:

So, I searched for a project.
Thanks to [Master of Olympus](http://www.masterofolympus.com/), a community behind Zeus is active.
With other developers, we decided to create the project [OpenOlympus](https://gitlab.com/openolympus/openolympus).

As for now, we're a small team of developer with the willing of re-creating the original game + add new features.

Many other projects exist with the same aim.
If you want to find a french community behind Zeus and discover the other projects, go to the Discord : https://discord.gg/eZkCFpk 
