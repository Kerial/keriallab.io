---
title: Can'App - OpenStreetMap à la rescousse
subtitle: Comment une volonté de libre facilite la vie
date: 2019-09-18
---

*Suite de l'[article de presentation de Can'App]( {{< ref "blog/canapp/2020-01-10-Presentation-Canapp.md" >}} ).*

Après avoir été confronté à ces premières questions plutôt techniques, j'ai une semaine assez chargée qui m'empêche de passer du temps sur le projet.
En revenant ainsi au bout d'une semaine, je préfère avant tout sauvegarder l'état dans lequel j'ai laissé le code.

<!--more-->

Hop, direction Gitlab, nouveau projet, etc.
Se pose alors la question de la license à mettre dessus.

Je me renseigne alors sur les différentes licenses qui permettent de protéger son code, tout en le laissant libre à n'importe qui de modifier et redistribuer ce code.
Je jette donc mon dévolu sur la license GPLv3.

Avec cette petite recherche sur les différentes licenses libres, je regarde également les projets libres qui existe et qui pourraient m'aider dans mon travail.
Je pense ainsi à [OpenStreetMap](https://www.openstreetmap.org/).

En parcourant cette base de données, je vois notamment la présence d'un tag "dog"...
:tada: Je vais donc pouvoir utiliser la base de données d'OpenStreetMap comme partie back !! :tada:

Morale: Contribuer au libre vous le rend bien...
